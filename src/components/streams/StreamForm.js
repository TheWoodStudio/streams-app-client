import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    maxWidth: "600px",
    margin: "0 auto",
    display: "block",
    marginTop: "5rem",
    [theme.breakpoints.down("xs")]: {
      margin: "0 1rem"
    }
  },
  form: {
    padding: "1rem"
  },
  field: {
    width: "100%",
    marginBottom: "1rem"
  },
  button: {
    width: "100%",
    marginTop: "1.5rem"
  }
});

class StreamForm extends Component {
  renderTextField = ({
    input,
    floatinglabeltext,
    meta: { touched, error },
    ...custom
  }) => {
    return (
      <div>
        <TextField
          autoComplete="off"
          floatinglabeltext={this.label}
          helperText={touched && error}
          error={touched && error ? true : false}
          {...input}
          {...custom}
        />
      </div>
    );
  };

  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.root}>
        <form
          className={classes.form}
          onSubmit={this.props.handleSubmit(this.onSubmit)}
        >
          {this.props.children}
          <Field
            name="title"
            component={this.renderTextField}
            label="Title"
            className={classes.field}
          />
          <Field
            name="description"
            component={this.renderTextField}
            label="Description"
            className={classes.field}
          />
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            type="submit"
          >
            Submit
          </Button>
        </form>
      </Paper>
    );
  }
}

const validate = formValues => {
  const errors = {};

  if (!formValues.title) {
    errors.title = "You must enter a title";
  }

  if (!formValues.description) {
    errors.description = "You must enter a description";
  }

  return errors;
};

export default reduxForm({
  form: "streamForm",
  validate
})(withStyles(styles)(StreamForm));
