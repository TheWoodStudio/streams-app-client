import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchStreams } from "./../../actions";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

import StreamItem from "./StreamItem";

import List from "@material-ui/core/List";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import StreamDelete from "./StreamDelete";

const styles = theme => ({
  root: {
    width: "100%"
  },
  flexTitle: {
    display: "flex",
    justifyContent: "space-between",
    padding: "0 1.5rem",
    alignItems: "center"
  },
  list: {
    maxWidth: 600
  }
});

class StreamList extends Component {
  state = {
    open: false
  };
  componentDidMount() {
    this.props.fetchStreams();
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  renderList = ({ streams }) => {
    return streams.map(stream => {
      return (
        <StreamItem
          key={stream.id}
          title={stream.title}
          description={stream.description}
          adminButtons={this.renderAdminButtons(stream)}
        />
      );
    });
  };

  renderAdminButtons = stream => {
    if (stream.userId === this.props.currentUserId) {
      return (
        <React.Fragment>
          <Link to={`/streams/edit/${stream.id}`}>
            <IconButton variant="outlined" color="secondary">
              <EditIcon />
            </IconButton>
          </Link>
          <IconButton
            variant="outlined"
            color="primary"
            onClick={this.handleOpen}
          >
            <DeleteIcon />
          </IconButton>
          <StreamDelete open={this.state.open} handleClose={this.handleClose} streamId={stream.id}/>
        </React.Fragment>
      );
    }
  };

  renderCreate = () => {
    if (this.props.isSignedIn) {
      return (
        <div>
          <Button variant="outlined" color="primary">
            <Link to="/streams/new">Create stream</Link>
          </Button>
        </div>
      );
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.flexTitle}>
          <h1>Streams List</h1>
          {this.renderCreate()}
        </div>
        <List className={classes.list}>{this.renderList(this.props)}</List>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    streams: Object.values(state.streams),
    currentUserId: state.auth.userId,
    isSignedIn: state.auth.isSignedIn
  };
};

export default connect(
  mapStateToProps,
  {
    fetchStreams
  }
)(withStyles(styles)(StreamList));
