import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LinkedCamera from '@material-ui/icons/LinkedCamera';
import Typography from '@material-ui/core/Typography';

const styles = ({
  inline: {
    display: 'inline',
  },
  icon: {
    marginRight: '1rem',
    fontSize: 32,
  },
})

const StreamItem = (props) => {
  const { classes } = props;
  return (
      <ListItem key={props.key} button component="li">
        <LinkedCamera className={classes.icon} />
        <ListItemText
          primary={props.title}
          secondary={
            <Typography component="span" className={classes.inline} color="textPrimary">
              {props.description}
            </Typography>
          }
        />
        {props.adminButtons}
      </ListItem>
  );
}

export default withStyles(styles)(StreamItem);
