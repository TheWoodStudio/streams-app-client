import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteStream } from "./../../actions";
import Modal from "./../Modal";

class StreamDelete extends Component {
  handleAgree = id => {
    this.props.deleteStream(id);
  };

  render() {
    return (
      <Modal
        title="Are you sure you want to delete this stream?"
        description="You can't undo this action."
        textButtonOk="Agree"
        textButtonClose="Disagree"
        open={this.props.open}
        onClose={this.props.handleClose}
        onAgree={() => this.handleAgree(this.props.streamId)}
      />
    );
  }
}

export default connect(
  null,
  { deleteStream }
)(StreamDelete);
