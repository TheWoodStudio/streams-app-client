import React, { Component } from "react";
import { connect } from "react-redux";
import { createStream } from "./../../actions";
import Typography from "@material-ui/core/Typography";
import StreamForm from "./StreamForm";

class StreamCreate extends Component {
  onSubmit = formValues => {
    this.props.createStream(formValues);
  };

  render() {
    return (
      <div>
        <StreamForm onSubmit={this.onSubmit}>
          <Typography component="h5" variant="h5" gutterBottom>
            Create a Stream
          </Typography>
        </StreamForm>
      </div>
    );
  }
}

export default connect(
  null,
  {
    createStream
  }
)(StreamCreate);
