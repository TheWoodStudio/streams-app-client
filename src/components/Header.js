import React from 'react';
import { Link } from 'react-router-dom';
import GoogleAuth from './../components/GoogleAuth';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    grow: {
      flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
    }
  });


const Header = (props) => {
    const { classes } = props;
    return ( 
        <AppBar>
            <Toolbar>
                <Typography variant="h6" color="inherit" className={classes.grow}>
                    Stream App
                </Typography>
                <Link to="/">
                    <Button variant="contained" color="primary" className={classes.button}>
                        Streams
                    </Button>
                </Link>
                <GoogleAuth />
            </Toolbar>
        </AppBar>
     );
}
 
export default withStyles(styles)(Header);