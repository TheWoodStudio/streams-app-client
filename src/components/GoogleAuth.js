import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from './../actions';
import Button from '@material-ui/core/Button';

class GoogleAuth extends Component {
    componentDidMount() {
        this.gapiLoad()
    }

    gapiLoad = () => {
        window.gapi.load('client:auth2', () => {
            window.gapi.client
            .init({
                clientId: '584448446422-tvm43l0teu0rt1gqoaejrlj2nu0me1pg.apps.googleusercontent.com',
                scope: 'email'
            })
            .then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get())
                this.auth.isSignedIn.listen(this.onAuthChange)
            })
        });
    }

    onAuthChange = (isSignedIn) => {
        if(isSignedIn) {
            this.props.signIn(this.auth.currentUser.get().getId());
        } else {
            this.props.signOut();
        }
    }

    onSignInClick = () => {
        this.auth.signIn()
    }

    onSignOutClick = () => {
        this.auth.signOut()
    }

    renderAuthButton = () => {
        if (this.props.isSignedIn === null) {
            return null;
        } else if (this.props.isSignedIn) {
            return (
                <Button variant="outlined" color="inherit" onClick={this.onSignOutClick}>
                    Sign Out
                </Button>
            );
        } else {
            return (
                <Button variant="outlined" color="inherit" onClick={this.onSignInClick}>
                    Login with Google
                </Button>
            );
        }
    }

    render() { 
        return ( 
            <React.Fragment>
                {this.renderAuthButton()}
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
     return {
         isSignedIn: state.auth.isSignedIn
     }
}
 
export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
